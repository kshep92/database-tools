import com.reviselabs.database.DataSource;
import com.typesafe.config.ConfigFactory;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Created by Kevin on 6/17/2016.
 */
public class DatabaseServiceTest {

    private DataSource dataSource;

    @Test
    public void testH2Detection() throws Exception {
        dataSource = new DataSource(); //Should log that we're connecting to an H2 Database;
        assertNotNull(dataSource.getConnection());
    }

    @Test
    public void testPostgreSQLDetection() throws Exception {
        dataSource = new DataSource(ConfigFactory.load("postgres"));
        assertNotNull(dataSource.getConnection());
    }

    @Test
    public void testSqlServerDetection() throws Exception {
        dataSource = new DataSource(ConfigFactory.load("sqlserver"));
        assertNotNull(dataSource.getConnection());

    }
}

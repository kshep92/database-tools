package mock;

import com.google.inject.Singleton;
import com.reviselabs.database.DatabaseService;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import java.util.Scanner;

/**
 * Created by Kevin on 6/15/2016.
 */
@Singleton
public class MockDatabase implements DatabaseService {

    private Sql2o sql2o;

    public MockDatabase() {
        Logger logger = LoggerFactory.getLogger(MockDatabase.class);
        logger.debug("Creating an H2 file system database...");
        Config conf = ConfigFactory.load();
        String url = conf.getString("db.url");
        String user = conf.getString("db.user");
        String password = conf.getString("db.password");
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        sql2o = new Sql2o(url, user, password);
        loadDataFromResource("testdb.sql");
        logger.debug("Database created.");
    }

    public Connection getConnection() {
        return sql2o.open();
    }


}

package mock;

import com.reviselabs.database.data.DAO;
import com.reviselabs.database.data.Row;
import models.User;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Kevin on 6/15/2016.
 */
public class MockDAO extends DAO {

    public User findById(int id) {
        return fetchOne((conn) -> {
            String sql = "select * from user where id = :id";
            return conn.createQuery(sql).addParameter("id", id).executeAndFetchFirst(converter);
        }).as(User.class);
    }

    public List<User> findAll() {
        List<Row> rows = fetchMany((conn) ->
            conn.createQuery("select * from user").executeAndFetch(converter));
        return rows.stream().map(elm -> elm.as(User.class)).collect(Collectors.toList());
    }

    public void update(User user) {
        executeStatement((conn) -> conn.createQuery("update user set name = :name, email = :email, age = :age where id = :id")
        .addParameter("name", user.getName())
        .addParameter("email", user.getEmail())
        .addParameter("age", user.getAge())
        .addParameter("id", user.getId())
        .executeUpdate());
    }

    public User findByEmail(String email) {
        return fetchOne((conn) ->
                conn.createQuery("select * from user where email = :email")
                        .addParameter("email", email)
                        .executeAndFetchFirst(converter))
                        .as(User.class);
    }

    public Row getCompositeUserRow() {
        return fetchOne((connection) -> {
            String sql = "select u.id, u.name, p.id as pet_id, p.name as pet_name from user u " +
                    "join pet p on p.owner_id = u.id " +
                    "where u.id = 1";
            return connection
                    .createQuery(sql).executeAndFetchFirst(converter);
        });
    }

    public List<Row> getCompositeUserRows() {
        return fetchMany((connection) -> {
            String sql = "select u.id, u.name, p.id as pet_id, p.name as pet_name from user u " +
                    "join pet p on p.owner_id = u.id " +
                    "order by u.name";
            return connection.createQuery(sql).executeAndFetch(converter);
        });
    }

    public Row getUserRow() {
        return fetchOne((conn) -> conn.createQuery("select * from user where id = 1").executeAndFetchFirst(converter));
    }

    public Row getPetRow() {
        return fetchOne((conn) -> conn.createQuery("select * from pet where owner_id = 1").executeAndFetchFirst(converter));
    }
}

import com.reviselabs.database.util.StringConverter;
import com.sun.org.apache.xpath.internal.operations.String;
import org.junit.Test;

import static org.junit.Assert.*;

/**

 **/

public class StringConverterTest {

    @Test
    public void testCamelCaseConversion() throws Exception {
        assertEquals(StringConverter.toCamelCase("PERSON_NAME"), "personName");
        assertEquals(StringConverter.toCamelCase("place_of_work"), "placeOfWork");
        assertEquals(StringConverter.toCamelCase("ID"), "id");
        assertEquals(StringConverter.toCamelCase("name"), "name");
        assertEquals(StringConverter.toCamelCase("invoiceNumber"), "invoiceNumber");
    }

    @Test
    public void testSnakeCaseConversion() throws Exception {
        assertEquals(StringConverter.toSnakeCase("personaNonGrata"), "persona_non_grata");
    }
}

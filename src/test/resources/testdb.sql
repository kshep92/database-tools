drop table if EXISTS user;
drop table IF EXISTS pet;
drop TABLE IF EXISTS customer;
drop TABLE IF EXISTS invoice;

/* DAO Tests */

create TABLE user(
  id INT AUTO_INCREMENT,
  name VARCHAR(50),
  email VARCHAR(50),
  age INT,
  birthday DATE,
  created_at TIMESTAMP DEFAULT now(),
  PRIMARY KEY(id)
);

CREATE TABLE pet(
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(20),
  owner_id INT,
  FOREIGN KEY(owner_id) REFERENCES user(id) ON DELETE CASCADE
);

INSERT INTO user(id, name, email, age, birthday) VALUES (1, 'Kevin', 'kevin@mail.com', 27, cast('1989-04-02' as DATE)  );
INSERT INTO user(id, name, email, age, birthday) VALUES (2, 'Melanie', 'melanie@hotmail.com', 26, cast('1990-06-23' as DATE));
INSERT INTO user(id, name, email, age, birthday) VALUES (3, 'Sharon', 'sharon@gmail.com', 32, '1958-10-01');
INSERT INTO user(id, name, email, age, birthday) VALUES (4, 'Derrick', 'dman22@mail.com', 26, '1998-05-16');

INSERT INTO pet (name, owner_id) VALUES ('Fido', 1);
INSERT INTO pet (name, owner_id) VALUES ('Mittens', 2);
INSERT INTO pet (name, owner_id) VALUES ('Charlie', 3);
INSERT INTO pet (name, owner_id) VALUES ('Slippy', 4);


/* ActiveRecord Tests */

CREATE TABLE customer (
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(20),
  email VARCHAR(100),
  address VARCHAR(200)
);

CREATE TABLE invoice (
  id INT PRIMARY KEY AUTO_INCREMENT,
  total DOUBLE,
  customer_id INT REFERENCES customer(id) ON DELETE CASCADE,
  purchase_date DATE,
  created_at DATETIME DEFAULT now()
);

INSERT INTO customer(id, name, email, address) VALUES (1, 'Kevin Sheppard', 'kevin@mail.com', 'Starfish Island');
INSERT INTO customer(id, name, email, address) VALUES (2, 'Sean Patterson', 'sean@mail.com', 'Vice Point');
INSERT INTO customer(id, name, email, address) VALUES (3, 'Grace Randolph', 'grace@mail.com', 'Washington Beach');

INSERT INTO invoice(total, customer_id, purchase_date) VALUES (250.75, 1, '2014-02-12');
INSERT INTO invoice(total, customer_id, purchase_date) VALUES (2311.55, 1, '2014-03-11');
INSERT INTO invoice(total, customer_id, purchase_date) VALUES (100.25, 1, '2015-06-08');
INSERT INTO invoice(total, customer_id, purchase_date) VALUES (96.19, 1, '2014-12-12');
INSERT INTO invoice(total, customer_id, purchase_date) VALUES (38.50, 1, '2014-04-02');




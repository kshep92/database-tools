package com.reviselabs.database;

import com.google.inject.AbstractModule;

/**
 * Created by Kevin on 6/15/2016.
 */
public class DatabaseInjector extends AbstractModule {

    private DatabaseService service;

    public DatabaseInjector(DatabaseService service) { this.service = service; }

    @Override
    protected void configure() {
        bind(DatabaseService.class).toInstance(service);
    }


}

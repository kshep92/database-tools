package com.reviselabs.database;

import com.typesafe.config.Config;
import com.zaxxer.hikari.HikariConfig;
import org.sql2o.Sql2o;

import java.io.InputStream;
import java.util.Scanner;

/**
 * Created by Kevin on 6/15/2016.
 */
public abstract class DataSourceBase {
    protected Sql2o sql2o;
    protected Config conf;
    protected HikariConfig hikari;

    protected boolean exists(String value) {
        return conf.hasPath(value) && !conf.getString(value).isEmpty();
    }

    protected String read(String value) {
        return conf.getString(value);
    }

    protected String read(String value, String defaultValue) {
        if(exists(value)) return read(value);
        else return defaultValue;
    }

    protected String readFile(InputStream inputStream) {
        Scanner scanner = new Scanner(inputStream);
        String content = "";
        while(scanner.hasNext()) content += scanner.nextLine();
        return content;
    }
}
